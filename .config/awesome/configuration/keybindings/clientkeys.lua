local gears = require("gears")
local awful = require("awful")

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "Vollbildmodus umschalten", group = "Client/Fenster"}),
    awful.key({ modkey, "Control" }, "c",      function (c) c:kill()                       end,
              {description = "Schließen", group = "Client/Fenster"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "Floating umschalten", group = "Client/Fenster"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "Client zum Master machen", group = "Client/Fenster"}),
    awful.key({ modkey, "Control"}, "o",      function (c) c:move_to_screen()               end,
              {description = "Client auf den anderen Bildschirm schieben", group = "Client/Fenster"}),
    awful.key({ modkey,}, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "Immer im Vordergrung umschalten", group = "Client/Fenster"}),
    awful.key({ modkey,}, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "Minimieren", group = "Client/Fenster"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "Maximieren umschalten", group = "Client/Fenster"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "Vertikal maximieren umschalten", group = "Client/Fenster"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "Horizontal maximieren umschalten", group = "Client/Fenster"})
)

return clientkeys
