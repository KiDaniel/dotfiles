-- Standard awesome library
--local awful = require('awful')
--require('awful.autofocus')
--local beautiful = require('beautiful')
--local hotkeys_popup = require('awful.hotkeys_popup').widget
--
--local modkey = require('configuration.keys.mod').modKey
--local altkey = require('configuration.keys.mod').altKey
--local apps = require('configuration.apps')

--pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
-- Widget and layout library
--local wibox = require("wibox")

-- Theme handling library
--local beautiful = require("beautiful")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")


-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key(
        {modkey,}, 'z',
        function()
            for _, c in ipairs(client.get()) do
                awful.titlebar.toggle(c)
            end
                     
        end,
        {description = 'Titelleisten umschalten', group = 'Client/Fenster'}
    ),

    awful.key(
        {},'XF86AudioRaiseVolume', 
        function() volume_widget:inc() end            
    ),

    awful.key(
        {}, 'XF86AudioLowerVolume', 
        function() volume_widget:dec() end
    ),

    awful.key(
        {}, 'XF86AudioMute', 
        function() volume_widget:toggle() end
    ),

    awful.key(
        { modkey,}, "s",
        hotkeys_popup.show_help,
        {description="Hotkeys anzeigen", group="Awesome"}
    ),
    
    awful.key(
        { modkey,}, "#",
        function()
            awful.spawn.with_shell("i3lock-fancy")
        end,
        {description="Bildschirm sperren", group="Awesome"}
    ),  

    awful.key(
        { modkey,}, "Left", 
        awful.tag.viewprev,
        {description = "Linker Tag", group = "Tag/Virtueller Desktop"}
    ),

    awful.key(
        { modkey,}, "Right", 
        awful.tag.viewnext,
        {description = "Rechter Tab", group = "Tag/Virtueller Desktop"}
    ),

    awful.key(
        { modkey,}, "Escape", 
        awful.tag.history.restore,
        {description = "Vorheriger Tab", group = "Tag/Virtueller Desktop"}
    ),

    awful.key({ modkey,A}, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "Fokus auf nächsten Client (nach Index)", group = "Client/Fenster"}
    ),

    awful.key({ modkey,}, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "Fokus auf vorherigen Client (nach Index)", group = "Client/Fenster"}
    ),

    awful.key(
        { modkey,}, "w", 
        function () 
            mymainmenu:show() 
        end,
        {description = "Menü anzeigen", group = "Awesome"}
    ),

-- Layout manipulation
    awful.key(
        { modkey, "Control"}, 
        "j", 
        function () 
            awful.client.swap.byidx(  1)    
        end,
        {description = "Client mit nächstem tauschen (nach Index)", group = "Client/Fenster"}
    ),

    awful.key(
        { modkey, "Control"}, "k", 
        function () 
            awful.client.swap.byidx( -1)    
        end,
        {description = "Client mit vorherigem tauschen (nach Index)", group = "Client/Fenster"}
    ),

    awful.key(
        { modkey, "Mod1"}, "j", 
        function () 
            awful.screen.focus_relative( 1) 
        end,
        {description = "Fokus auf nächsten Bildschirm", group = "Bildschirm"}
    ),

    awful.key(
        { modkey, }, "o", 
        function () 
            awful.screen.focus_relative(-1) 
        end,
        {description = "Fokus auf vorherigem Bildschirm", group = "Bildschirm"}
    ),

    awful.key(
        { modkey,}, "u", 
        awful.client.urgent.jumpto,
        {description = "Zu dringendem Client wechseln", group = "Client/Fenster"}
    ),

    awful.key(
        { modkey,}, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "Zu letztem Client zurück", group = "Client/Fenster"}),

-- Standard program
    awful.key(
        { modkey,}, "t", 
        function () 
            awful.spawn(terminal) 
        end,
        {description = "Terminal öffnen", group = "Launcher"}
    ),

    awful.key(
        { modkey,}, "b", 
        function () 
            awful.spawn(browser) 
        end,
        {description = "Browser öffnen", group = "Launcher"}
    ),

    awful.key(
        { modkey,}, "x", 
        function () 
            awful.spawn(pw_manager) 
        end,
        {description = "Passwortmanager öffnen", group = "Launcher"}
    ),

    awful.key(
        { modkey,}, "v", 
        function () 
            awful.spawn(editor) 
        end,
        {description = "Editor öffnen", group = "Launcher"}
    ),

    awful.key(
        { modkey,}, ",", 
        function () 
            awful.spawn(email) 
        end,
        {description = "E-Mail Programm öffnen", group = "Launcher"}
    ),
    
    awful.key(
        { modkey, "Shift"}, "q", 
        awesome.quit,
        {description = "Awesome beenden", group = "Awesome"}
    ),

    awful.key(
        { modkey, "Control" }, "r", 
        awesome.restart,
        {description = "Awesome neu laden", group = "Awesome"}
    ),    

    awful.key(
        { modkey,}, "l",     
        function () 
            awful.tag.incmwfact( 0.05)          
        end,
        {description = "increase master width factor", group = "layout"}
    ),

    awful.key(
        { modkey,}, "h",     
        function () 
            awful.tag.incmwfact(-0.05)          
        end,
        {description = "decrease master width factor", group = "layout"}
    ),

    awful.key(
        { modkey, "Shift"}, "h",     
        function () 
            awful.tag.incnmaster( 1, nil, true) 
        end,
        {description = "increase the number of master clients", group = "layout"}
    ),

    awful.key(
        { modkey, "Shift"}, "l",     
        function () 
            awful.tag.incnmaster(-1, nil, true) 
        end,
        {description = "decrease the number of master clients", group = "layout"}
    ),

    awful.key(
        { modkey, "Control" }, "h",     
        function () 
            awful.tag.incncol( 1, nil, true)    
        end,
        {description = "increase the number of columns", group = "layout"}
    ),

    awful.key(
        { modkey, "Control" }, "l",     
        function () awful.tag.incncol(-1, nil, true)    
        end,
        {description = "decrease the number of columns", group = "layout"}
    ),

    awful.key(
        { modkey,}, "space", 
        function () 
            awful.layout.inc( 1)                
        end,
        {description = "Nächstes Layout wählen", group = "Layout"}
    ),

    awful.key(
        { modkey, "Shift"}, "space", 
        function () 
            awful.layout.inc(-1)                
        end,
        {description = "Vorheriges Layout wählen", group = "Layout"}
    ),

    awful.key({ modkey, "Control" }, "n",
        function ()
            local c = awful.client.restore()
            if c then
                c:emit_signal(
                    "request::activate", 
                    "key.unminimize", 
                    {raise = true}
                )
            end
        end,
        {description = "restore minimized", group = "client"}
    ),

    awful.key(
        { modkey },"r",
        function () 
            awful.util.spawn("rofi -show drun") 
        end,
        {description = "Launcher aufrufen", group = "Launcher"}
    ),

    awful.key({ modkey, "Control" }, "x",
        function ()
            awful.prompt.run {
                prompt       = "Run Lua code: ",
                textbox      = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        {description = "Lua Promt", group = "Awesome"}
    ),

    -- Menubar
    awful.key(
        { modkey }, "p", 
        function() 
            menubar.show() 
        end,
        {description = "Menüleiste anzeigen", group = "Launcher"}
    )
)

for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        
        awful.key(
            { "Control" }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            {description = "Zeige nur Tag #"..i, group = "Tag/Virtueller Desktop"}
        ),

        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            {description = "Zeige auch Tag #" .. i, group = "Tag/Virtueller Desktop"}
        ),

        awful.key({ modkey, "Control" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            {description = "Setze nur Tag # für Client mit Fokus"..i, group = "Tag/Virtueller Desktop"}
        ),

        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            {description = "Setze Tag # für Client mit Fokus" .. i, group = "Tag/Virtueller Desktop"}
        )
    )
end



return globalkeys
