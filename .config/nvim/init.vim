let mapleader=" "

syntax on
"Ruft vimrc in Projektordner zuerst auf
set exrc

"Taboptions
set tabstop=4 softtabstop=4
set shiftwidth=4
set smartindent
set expandtab

"Zeilennummern
set nu
set relativenumber


"Disable Highlight after searching
set nohlsearch
"Nachgucken
set hidden

"Keine nervigen Töne
set noerrorbells

"Undoverhalten
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile

"Incemental Search
set incsearch

"Wann soll Vim anfangen zu scrollen
set scrolloff=8

"Spalte für Fehlermeldungen
set signcolumn=yes

"Signal für Länge
"set colorcolumn=80

call plug#begin('~/.vim/plugged')
    Plug 'arcticicestudio/nord-vim' 
    Plug 'mbbill/undotree'
    Plug 'scrooloose/nerdtree'
    Plug 'junegunn/goyo.vim'
    Plug 'itchyny/lightline.vim'
call plug#end()

set termguicolors
colorscheme nord
set background=dark

"Einfachere split Navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"Higlight
set cursorline 

" Enable disable Goyo
map <leader>g :Goyo<CR>

" Fix default autocompletion
set wildmode=longest,list,full

" Fix splitting
set splitbelow splitright

" Setup Nerdtree
" Toggle On/Off
nnoremap <leader>n :NERDTreeFocus<CR>
" Start NERDTree and put the cursor back in the other window.
" autocmd VimEnter * NERDTree | wincmd p

" Undotree Hotkey
nnoremap <leader>u :UndotreeToggle<CR>

"Setup Lightline
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ }
