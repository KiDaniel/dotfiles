-- Imports
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local clickable_container = require('widget.material.clickable-container')
local mat_icon_button = require('widget.material.icon-button')
local mat_icon = require('widget.material.icon')
local icons = require('theme.icons')
local dpi = require('beautiful').xresources.apply_dpi


-- Create and setup Widgets
    local mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })
    local mykeyboardlayout = awful.widget.keyboardlayout()
    local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
    local logout_menu_widget = require("awesome-wm-widgets.logout-menu-widget.logout-menu")
    local net_widgets = require("net_widgets")
    local mytextclock = wibox.widget.textclock('<span font="Roboto Mono 12">%a, %e. %B - %H:%M Uhr</span>')
   
    --local net_wireless = net_widgets.wireless({interface   = "wlp3s0", onclick     = terminal .. " -e sudo wifi-menu" })
    local net_internet = net_widgets.internet({indent = 0, timeout = 5})
    --local net_wired = net_widgets.indicator({interfaces  = {"enp2s0", "another_interface", "and_another_one"}, timeout  = 5--})
    
    local systray = wibox.widget.systray()
        systray:set_horizontal(true)

    local add_button = mat_icon_button(mat_icon(icons.plus, dpi(20)))
    add_button:buttons(
      gears.table.join(
        awful.button(
          {},
          1,
          nil,
          function()
            awful.spawn.with_shell(
              "rofi -show drun"
            )
          end
        )
      )
    )

    local month_calendar = awful.widget.calendar_popup.month({
        screen = s,
        start_sunday = false,
        week_numbers = true
      })

    month_calendar:attach(mytextclock, "tr", {on_hover = false})
    
    local separator = wibox.widget {
        widget = wibox.widget.separator,
        orientation = "vertical",
        forced_width = dpi(5)
    }
-- Create a wibox for each screen and add it
    local taglist_buttons = gears.table.join(
                        awful.button({ }, 1, function(t) t:view_only() end),
                        awful.button({ modkey }, 1, function(t)
                                                if client.focus then
                                                    client.focus:move_to_tag(t)
                                                end
                                            end),
                        awful.button({ }, 3, awful.tag.viewtoggle),
                        awful.button({ modkey }, 3, function(t)
                                                if client.focus then
                                                    client.focus:toggle_tag(t)
                                                end
                                            end),
                        awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                        awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                    )

    local tasklist_buttons = gears.table.join(
                        awful.button({ }, 1, function (c)
                            if c == client.focus then
                                c.minimized = true
                            else
                                c:emit_signal("request::activate","tasklist",{raise = true})
                            end
                        end),

                        --awful.button({}, 2, function(c)
                        --    c.kill(c)
                        --end),

                        awful.button({ }, 3, function()
                            awful.menu.client_list({ theme = { width = 250 } })
                        end),

                        awful.button({ }, 4, function ()
                            awful.client.focus.byidx(1)
                        end),

                        awful.button({ }, 5, function ()
                            awful.client.focus.byidx(-1)
                        end))

-- Funktion, die Toppanel generiert
local TopPanel = function(s)
    -- Each screen has its own tag table.
        awful.tag({ "1", "2", "3", "4 - E-Mail", "5 - Musik", "6 - Passwort", "7", "8", "9" }, s, awful.layout.layouts[1])
    -- Create a promptbox for each screen
        s.mypromptbox = awful.widget.prompt()
    
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
        s.mylayoutbox = awful.widget.layoutbox(s)
        s.mylayoutbox:buttons(gears.table.join(
                               awful.button({ }, 1, function () awful.layout.inc( 1) end),
                               awful.button({ }, 3, function () awful.layout.inc(-1) end),
                               awful.button({ }, 4, function () awful.layout.inc( 1) end),
                               awful.button({ }, 5, function () awful.layout.inc(-1) end)))
        
    -- Create a taglist widget
        s.mytaglist = awful.widget.taglist {
            screen  = s,
            filter  = awful.widget.taglist.filter.all,
            buttons = taglist_buttons,
            layout = wibox.layout.fixed.horizontal,
            style = {
                width = 20
            }
        }
    
    -- Create a tasklist widget
        s.mytasklist = awful.widget.tasklist {
            screen  = s,
            filter  = awful.widget.tasklist.filter.currenttags,
            layout = wibox.layout.fixed.horizontal,
            buttons = tasklist_buttons,
            style = {
                spacing = 0,
                align = "left",
            },
        }
    
    -- Create the wibox
        s.panel = awful.wibar(
            {
                position = "top", 
                screen = s ,
                height = 25
            }
        )
    
    -- Add widgets to the wibox
        s.panel:setup {
            layout = wibox.layout.align.horizontal,
    
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
    
                logout_menu_widget(),
                separator,
                s.mytaglist,
                separator,
                s.mypromptbox,
                s.mytasklist,
                add_button
            },
            
            nil, -- Middle widget
            
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,

                separator,
                volume_widget(),
                mykeyboardlayout,
                systray,                
                net_internet,
                s.mylayoutbox,
                separator,
                mytextclock,
                separator,
            },
        }
    end

return TopPanel