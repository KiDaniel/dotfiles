---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
--local themes_path = gfs.get_themes_dir()
local themes_path = string.format("%s/.config/awesome/themes/", os.getenv("HOME"), "default")

local theme = {}
local nord0   = "#2e3440"
local nord1   = "#3b4252"
local nord2   = "#434c5e"
local nord3   = "#4c566a"

local nord4   = "#d8dee9"
local nord5   = "#e5e9f0"
local nord6   = "#eceff4"

local nord7   = "#8fbcbb"
local nord8   = "#88c0d0"
local nord9   = "#81a1c1"
local nord10  = "#5e81ac"

local nord11  = "#bf616a"
local nord12  = "#d08770"
local nord13  = "#ebcb8b"
local nord14  = "#a3be8c"
local nord15  = "#b48ead"



theme.font          = "sans 8"

-- Colors 
theme.bg_normal     = nord0
theme.bg_focus      = nord7
theme.bg_urgent     = nord11
theme.bg_minimize   = nord15
theme.bg_systray    = nord0
theme.titlebar_bg_focus = nord7
theme.titlebar_bg_normal = nord0
theme.fg_normal     = nord4
theme.fg_focus      = nord4
theme.fg_urgent     = nord4
theme.fg_minimize   = nord4


theme.useless_gap   = 4
theme.border_width = 4
theme.systray_icon_spacing = 2
theme.border_marked = nord14

theme.border_focus = nord7
theme.border_normal = nord0

theme.tasklist_bg_normal = nord3

theme.taglist_bg_occupied = nord3

theme.separator_color = nord0
-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
    -- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
    -- tasklist_[bg|fg]_[focus|urgent]
    -- titlebar_[bg|fg]_[normal|focus]
    -- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
    -- mouse_finder_[color|timeout|animate_timeout|radius|factor]
    -- prompt_[fg|bg|fg_cursor|bg_cursor|font]
    -- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
    --theme.taglist_bg_focus = "#ff0000"


-- Generate taglist squares:
local taglist_square_size = 4
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
    -- notification_font
    -- notification_[bg|fg]
    -- notification_[width|height|margin]
    -- notification_[border_color|border_width|shape|opacity]
    theme.notification_border_color = nord11
    theme.notification_border_width = 10

-- Variables set for theming the menu:
    -- menu_[bg|fg]_[normal|focus]
    -- menu_[border_color|border_width]
    theme.menu_submenu_icon = themes_path.."default/submenu.png"
    theme.menu_height = dpi(15)
    theme.menu_width  = dpi(100)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
    --theme.bg_widget = "#cc0000"

-- Zu importierende Bilder
theme.titlebar_close_button_normal = themes_path.."default/Icons/dialog-close.svg"
theme.titlebar_close_button_focus  = themes_path.."default/Icons/dialog-close.svg"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/Icons/view-sort-ascending.svg"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/Icons/view-sort-ascending.svg"
theme.titlebar_ontop_button_normal_active = themes_path.."default/Icons/view-sort-descending.svg"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/Icons/view-sort-descending.svg"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/Icons/window-pin.svg"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/Icons/window-pin.svg"
theme.titlebar_sticky_button_normal_active = themes_path.."default/Icons/window-unpin.svg"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/Icons/window-unpin.svg"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/Icons/view-split-left-right.svg"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/Icons/view-split-left-right.svg"
theme.titlebar_floating_button_normal_active = themes_path.."default/Icons/transform-shear-left.svg"
theme.titlebar_floating_button_focus_active  = themes_path.."default/Icons/transform-shear-left.svg"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/Icons/zoom-in.svg"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/Icons/zoom-in.svg"
theme.titlebar_maximized_button_normal_active = themes_path.."default/Icons/zoom-out-large.svg"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/Icons/zoom-out-large.svg"

theme.wallpaper = "~/.config/awesome/themes/default/Tree_Roots.jpg"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme
