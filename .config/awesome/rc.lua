-- Imports
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
require("awful.autofocus")
local naughty = require("naughty")
local hotkeys_popup = require("awful.hotkeys_popup")

-- Theme
local chosen_theme = "default"
local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)
beautiful.init(theme_path)

-- Standardprogramme
terminal = "konsole"
editor = "code"
editor_cmd = terminal .. " -e " .. editor
modkey = "Mod4" -- Mod4 ist Super/Windowstaste
browser = "firefox"
pw_manager = "keepassxc"
email = "thunderbird"

-- Deutsch
os.setlocale(os.getenv("LANG"))

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Notwendig ??
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end




-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left,
    --awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --wful.layout.suit.spiral.dwindle,
    --awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    awful.layout.suit.floating,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}


-- Rechtsklickmenü einrichten
require('configuration.menu')

-- Keybindings
--mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
root.keys(require('configuration.keybindings.global'))
require('configuration.keybindings.clientkeys')
require('configuration.keybindings.clientbuttons')

-- Bildschirme einrichten
    local function set_wallpaper(s)
        -- Wallpaper
        if beautiful.wallpaper then
            local wallpaper = beautiful.wallpaper
            -- If wallpaper is a function, call it with the screen
            if type(wallpaper) == "function" then
                wallpaper = wallpaper(s)
            end
            gears.wallpaper.maximized(wallpaper, s, true)
        end
    end

    -- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
    screen.connect_signal("property::geometry", set_wallpaper)

    local toppanel = require("configuration.toppanel")

    awful.screen.connect_for_each_screen(function(s)
        -- Wallpaper
        set_wallpaper(s)
        toppanel(s)
    end)



--Regeln
    awful.rules.rules = require("configuration.rules")

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Dynamisch Opacity einstellen
-- client.connect_signal("focus", function(c)
--    c.border_color = beautiful.border_focus
--    c.opacity = 1
--  end)
-- client.connect_signal("unfocus", function(c)
--       c.border_color = beautiful.border_normal
--       c.opacity = 0.95
--  end)

-- Automatisch Fokus bei Hover wechseln
--client.connect_signal("mouse::enter", function(c)
 --   c:emit_signal("request::activate", "mouse_enter", {raise = false})
--end)

client.connect_signal("focus", 
    function(c) 
        c.border_color = beautiful.border_focus
        c.border_width = beautiful.border_width
    end
)

client.connect_signal("unfocus", 
    function(c) 
        c.border_color = beautiful.border_normal
        c.border_width = beautiful.border_width
    end)

client.connect_signal("property::floating", 
    function(c)
        if c.floating then
            awful.titlebar.show(c)
        else
            awful.titlebar.hide(c)
        end
end)

awful.tag.attached_connect_signal(nil, "property::layout", 
    function (t)
        local float = t.layout.name == "floating"
        for _,c in pairs(t:clients()) do
            if float then
                awful.titlebar.show(c)
            else
                awful.titlebar.hide(c)
            end
        end
  end)

-- }}}

-- Autostart
require('configuration.autostart')